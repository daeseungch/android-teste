package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    String mTitle[] = {
            "Filme 1",
            "Filme 2",
            "Filme 3",
            "Filme 4",
            "Filme 5",
            "Filme 6",
            "Filme 7",
            "Filme 8",
            "Filme 9",
            "Filme 10",
    };
    String mDescription[] ={
            "Categoria 1",
            "Categoria 2",
            "Categoria 3",
            "Categoria 4",
            "Categoria 5",
            "Categoria 6",
            "Categoria 7",
            "Categoria 8",
            "Categoria 9",
            "Categoria 10",
    };
    int images[] = {
            R.drawable.a1,
            R.drawable.a2,
            R.drawable.a3,
            R.drawable.a4,
            R.drawable.a5,
            R.drawable.a6,
            R.drawable.a7,
            R.drawable.a8,
            R.drawable.a9,
            R.drawable.a10,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.list_view);

        MyAdapter adapter = new MyAdapter(this, mTitle, mDescription, images);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0){
                    Intent myIntent = new Intent(view.getContext(), Sinopse0.class);
                    startActivityForResult(myIntent, 0);
                }

                if (i == 1){
                    Intent myIntent = new Intent(view.getContext(), Sinopse1.class);
                    startActivityForResult(myIntent, 0);
                }

                if (i == 2){
                    Intent myIntent = new Intent(view.getContext(), Sinopse2.class);
                    startActivityForResult(myIntent, 0);
                }

                if (i == 3){
                    Intent myIntent = new Intent(view.getContext(), Sinopse3.class);
                    startActivityForResult(myIntent, 0);
                }

                if (i == 4){
                    Intent myIntent = new Intent(view.getContext(), Sinopse4.class);
                    startActivityForResult(myIntent, 0);
                }

                if (i == 5){
                    Intent myIntent = new Intent(view.getContext(), Sinopse5.class);
                    startActivityForResult(myIntent, 0);
                }

                if (i == 6){
                    Intent myIntent = new Intent(view.getContext(), Sinopse6.class);
                    startActivityForResult(myIntent, 0);
                }

                if (i == 7){
                    Intent myIntent = new Intent(view.getContext(), Sinopse7.class);
                    startActivityForResult(myIntent, 0);
                }

                if (i == 8){
                    Intent myIntent = new Intent(view.getContext(), Sinopse8.class);
                    startActivityForResult(myIntent, 0);
                }

                if (i == 9){
                    Intent myIntent = new Intent(view.getContext(), Sinopse9.class);
                    startActivityForResult(myIntent, 0);
                }


            }
        });
    }

    class MyAdapter extends ArrayAdapter<String> {
        Context context;
        String rTitle[];
        String rDescription[];
        int rImgs[];

        MyAdapter(Context c, String title[], String description[], int imgs[]){
            super(c, R.layout.customlayout, R.id.textView1, title);
            this.context = c;
            this.rTitle = title;
            this.rDescription = description;
            this.rImgs = imgs;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.customlayout, parent, false);
            ImageView images = row.findViewById(R.id.image);
            TextView myTitle = row.findViewById(R.id.textView1);
            TextView myDescription = row.findViewById(R.id.textView2);

            images.setImageResource(rImgs[position]);
            myTitle.setText(rTitle[position]);
            myDescription.setText(rDescription[position]);

            return row;
        }
    }
}